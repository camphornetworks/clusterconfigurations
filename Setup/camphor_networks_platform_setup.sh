#!/usr/bin/env bash
#
# Copyright (C) Camphor Networks, Inc - All Rights Reserved
# Unauthorized copying of this file via any medium is strictly prohibited
# Proprietary and confidential
# Written by Ananth Suryanarayana <ananth@camphornetworks.com>, June 2021
# https://camphornetworks.com/camphor-networks-platform-terms-of-use-and-privacy-policy/
# mailto:info@camphornetworks.com?subject=copyright
#

set -ex

# BEGIN https://gitlab.com/camphornetworks/clusterconfigurations/-/wikis/CamphorNetworksPlatformSetup

# Run as root (superuser)
if [[ $(whoami) != "root" ]]; then
    echo Please run as superuser root by running \"sudo su -\" first.
    exit 1
fi

# Set environment
function set_environment () {

    # mailto:info@camphornetworks.com?subject=free-trial to following 3 secrets. You get one month free trial as well!
    export CAMPHOR_STORAGE_PATH="${CAMPHOR_STORAGE_PATH:-<storage>}"
    export CAMPHOR_LICENSE="${CAMPHOR_LICENSE:-<license>}"
    export CAMPHOR_CUSTOMER_SECRET="${CAMPHOR_CUSTOMER_SECRET:-<secret>}"

    # Update these to suit to your environment
    export CAMPHOR_EMAIL="${CAMPHOR_EMAIL:-<email>}"

    # At least 8 characters with a mix of lower and upper case letters.
    export CAMPHOR_PASSWORD="${CAMPHOR_PASSWORD:-<password>}"

    # Directory where camphor setup software will be installed
    export CAMPHOR_INSTALL_DIR="${CAMPHOR_INSTALL_DIR:-$HOME/camphor_install}"

    # Set this to true in order to directly install camphor into the local system as a single node setup.
    export CAMPHOR_LOCAL_INSTALL="${CAMPHOR_LOCAL_INSTALL:-FALSE}"

    # Set cluster name.
    export CAMPHOR_CLUSTER_NAME="${CAMPHOR_CLUSTER_NAME:-cluster1}"

    # Set camphor software release to install
    export CAMPHOR_RELEASE="${CAMPHOR_RELEASE:-latest}"

    # Set any proxy required to reach out to Internet
    export CAMPHOR_PROXY_SERVER="${CAMPHOR_PROXY_SERVER}"

    # Add all noproxy domains as applicable
    export CAMPHOR_NOPROXY_DOMAIN="${CAMPHOR_NOPROXY_DOMAIN:-*.local}"

    # Set Pod IPv4 Network
    export CAMPHOR_K8POD_NETWORK="${CAMPHOR_K8POD_NETWORK:-10.245.0.0/16}"

    # Set Service IPv4 Network
    export CAMPHOR_K8SERVICE_NETWORK="${CAMPHOR_K8SERVICE_NETWORK:-10.112.0.0/12}"

    # Set DNS IP
    export CAMPHOR_K8DNSIP="${CAMPHOR_K8DNSIP:-10.112.0.10}"
}

function usage () {
    set_environment

    # Download and executre camphor setup installer bootstrap script.
    curl -k --output ~/camphor_setup_installer.sh https://cn-pb-camphor-$CAMPHOR_STORAGE_PATH.s3.us-east-2.amazonaws.com/camphor_setup_installer.sh
    chmod +x ~/camphor_setup_installer.sh
    ~/camphor_setup_installer.sh

    if [[ "$CAMPHOR_LOCAL_INSTALL" != "TRUE" ]]; then
        # You can use https://<yourIP>:8443 to use Browser based easy and cozy Camphor VSCode environment!
        D="docker exec -i -e CAMPHOR_STORAGE_PATH -e CAMPHOR_LICENSE -e CAMPHOR_CUSTOMER_SECRET -e CAMPHOR_EMAIL -e CAMPHOR_PASSWORD -e CAMPHOR_PROXY_SERVER -e CAMPHOR_NOPROXY_DOMAIN -e CAMPHOR_INSTALL_DIR -e CAMPHOR_LOCAL_INSTALL -e CAMPHOR_RELEASE -e CAMPHOR_K8_MASTER -e CAMPHOR_K8_MASTER_PUBLIC_IP -e CAMPHOR_K8_COMPUTE -e CAMPHOR_K8_COMPUTE_PUBLIC_IP camphor_installer bash -c"
        while ! $D "ls /var/lib/camphor/camphor_setup.pyc"; do  # Wait until camphor software becomes ready
            sleep 30
        done
        sleep 5

        $D "cd /root/camphor/testbeds && source /root/.venv/bin/activate && python3 /var/lib/camphor/camphor_setup.pyc --cluster=$CAMPHOR_CLUSTER_NAME --oper=list"
        # $D "cd /root/camphor/testbeds && source /root/.venv/bin/activate && python3 /var/lib/camphor/camphor_setup.pyc --cluster=$CAMPHOR_CLUSTER_NAME --oper=create"
        # $D "cd /root/camphor/testbeds && source /root/.venv/bin/activate && python3 /var/lib/camphor/camphor_setup.pyc --cluster=$CAMPHOR_CLUSTER_NAME --oper=destroy"
    else
        # Run following commands inside a terminal to start playing with camphor clusters!
        cd /root/camphor/testbeds
        source /root/.venv/bin/activate
        python3 /var/lib/camphor/camphor_setup.pyc --cluster=$CAMPHOR_CLUSTER_NAME --oper=list
        # python3 /var/lib/camphor/camphor_setup.pyc --cluster=$CAMPHOR_CLUSTER_NAME --oper=create
        # python3 /var/lib/camphor/camphor_setup.pyc --cluster=$CAMPHOR_CLUSTER_NAME --oper=destroy
    fi
}

usage
